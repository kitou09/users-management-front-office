module.exports = {
  publicPath: process.env.NODE_ENV === "production" ? "static/" : "/",
  pluginOptions: {
    quasar: {
      importStrategy: "kebab",
      rtlSupport: false,
    },
  },
  transpileDependencies: ["quasar", "vue-meta"],
};
