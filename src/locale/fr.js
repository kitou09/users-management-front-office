export default {
  isoName: "fr",
  nativeName: "Français",
  label: {
    clear: "Effacer",
    ok: "OK",
    cancel: "Annuler",
    close: "Fermer",
    set: "Régler",
    select: "Sélectionner",
    reset: "Réinitialiser",
    remove: "Supprimer",
    update: "Mettre à jour",
    create: "Créer",
    search: "Rechercher",
    filter: "Filtrer",
    refresh: "Rafraîchir",
  },
  date: {
    days: "Dimanche_Lundi_Mardi_Mercredi_Jeudi_Vendredi_Samedi".split("_"),
    daysShort: "Dim_Lun_Mar_Mer_Jeu_Ven_Sam".split("_"),
    months:
      "Janvier_Février_Mars_Avril_Mai_Juin_Juillet_Août_Septembre_Octobre_Novembre_Décembre".split(
        "_"
      ),
    monthsShort: "Jan_Fev_Mar_Avr_Mai_Juin_Jui_Aou_Sep_Oct_Nov_Dec".split("_"),
    headerTitle: (date) =>
      new Intl.DateTimeFormat("fr", {
        weekday: "short",
        day: "numeric",
        month: "short",
      }).format(date),
    firstDayOfWeek: 1, // 0-6, 0 - Sunday, 1 Monday, ...
    format24h: true,
    pluralDay: "jours",
  },
  table: {
    noData: "Aucune donnée à afficher",
    noResults: "Aucune donnée trouvée",
    loading: "Chargement...",
    selectedRecords: (rows) =>
      rows > 0
        ? rows +
          " " +
          (rows === 1 ? "ligne sélectionnée" : "lignes sélectionnées") +
          "."
        : "Aucune ligne sélectionnée.",
    recordsPerPage: "Lignes par page :",
    allRows: "Tous",
    pagination: (start, end, total) => start + "-" + end + " sur " + total,
    columns: "Entêtes",
  },
  editor: {
    url: "URL",
    bold: "Gras",
    italic: "Italique",
    strikethrough: "Barré",
    underline: "Souligné",
    unorderedList: "Liste non ordonnée",
    orderedList: "Liste ordonnée",
    subscript: "Indice",
    superscript: "Exposant",
    hyperlink: "Hyperlien",
    toggleFullscreen: "Basculer en plein écran",
    quote: "Citation",
    left: "Aligner à gauche",
    center: "Aligner au centre",
    right: "Aligner à droite",
    justify: "Justifier",
    print: "Imprimer",
    outdent: "Diminuer l'indentation",
    indent: "Augmenter l'indentation",
    removeFormat: "Supprimer la mise en forme",
    formatting: "Mise en forme",
    fontSize: "Taille de police",
    align: "Aligner",
    hr: "Insérer une règle horizontale",
    undo: "Annuler",
    redo: "Refaire",
    heading1: "Titre 1",
    heading2: "Titre 2",
    heading3: "Titre 3",
    heading4: "Titre 4",
    heading5: "Titre 5",
    heading6: "Titre 6",
    paragraph: "Paragraphe",
    code: "Code",
    size1: "Très petit",
    size2: "Petit",
    size3: "Normal",
    size4: "Moyenne",
    size5: "Grand",
    size6: "Très grand",
    size7: "Maximum",
    defaultFont: "Police par défaut",
    viewSource: "Voir la source",
  },
  tree: {
    noData: "Aucun nœud à afficher",
    noResults: "Aucun nœud trouvé",
  },
  appName: "Ges Users",
  exception: {
    pages: {
      403: "Access denied. Please contact the administrator.",
      404: "Sorry, Page not found",
    },
    btn: {
      back: "Accueil",
    },
  },
  btn: {
    close: {
      title: "Fermer",
    },
    clear: {
      title: "Effacer",
    },
    search: {
      title: "Rechercher",
      title2: "Filtrer",
    },
    refresh: {
      title: "Actualiser",
    },
    save: {
      title: "Enregistrer",
      title2: "Valider",
    },
    logout: {
      title: "Cliquez pour vous déconnecter",
    },
  },
  message: {
    validations: {
      title: "Les champs contenant * sont requis.",
      numeric: "Seul les nombres positifs sont autorisés",
      email: "Email mal formatée",
      required: "Ce champ {property} est requis.",
      minLength:
        "Le champ {property} a une valeur de '{model}', mais il doit avoir une longueur minimale de {min}.",
      isUnique: "La valeur {model} existe déjà!",
    },
    error: {
      500: "Erreur survenue",
      400: "Le formulaire contient des erreurs",
      401: "Non autorisé",
    },
  },
  security: {
    title: "Connexion",
    description: "Entrez vos identifiants pour vous connecter",
    cols: {
      username: {
        title: "Nom d'utilisateur",
        required: "Nom d'utilisateur requis",
      },
      password: {
        title: "Mot de passe",
        required: "Mot de passe requis",
      },
    },
    form: {
      submit: "Connexion",
      bad_credentials: "Nom utilisateur ou mot de passe incorrect",
    },
    btn: {
      login: "Connexion",
    },
  },
  toolbar: {
    menu: {
      user: "Utilisateurs",
      account: "Mon compte",
    },
  },
  genders: {
    Female: "Femme",
    Male: "Homme",
  },
  operators: {
    lt: "Inférieur",
    lte: "Inférieur ou égal",
    gt: "Supérieur ou égal",
    gte: "Supérieur ou égal",
    exact: "Egal",
    between: "Entre",
  },
  user: {
    title: "Utilisateurs",
    subtitle: {
      title: "Vue générale sur les utilisateurs",
      title2: "Nouveau utilisateur",
      title3: "Editer l'utilisateur",
      title4: "Suppression",
      title5: "Mises à jours des services de l'utilisateur",
      title6: "Rechercher",
      title7: "Détails",
    },
    btn: {
      new: "Nouveau utilisateur",
      tooltip: {
        edit: {
          title: "Cliquez pour editer",
        },
        delete: {
          title: "Cliquez pour supprimer",
        },
        view: {
          title: "Cliquez pour voir les détails",
        },
      },
    },
    cols: {
      action: {
        title: "Actions",
      },
      created: {
        title: "Fait le",
      },
      operator: {
        title: "Critère sur âge",
      },
      username: {
        title: "Nom d'utilisateur *",
        title2: "Nom d'utilisateur",
        required: "Nom d'utilisateur requis",
        length: "Le nom d'utilisateur doit contenir {min} à {max} caractères",
        alphaNum: "Seul les caractères alphanumérique sont autorisé",
        isUnique: "Nom d'utilisateur existant!.",
      },
      password: {
        title: "Mot de passe *",
        title2: "Mot de passe",
        required: "Mot de passe requis",
        length: "Mot de passe doit contenir {min} à {max} caractères",
      },
      repeatPassword: {
        title: "Repéter le mot de passe *",
        title2: "Repéter le mot de passe",
        sameAs: "Mot de passe saisie non conforme",
      },
      firstName: {
        title: "Nom *",
        title2: "Nom",
        required: "Nom requis",
        length: "Nom doit contenir {min} à {max} caractères",
      },
      lastName: {
        title: "Prenom",
      },
      gender: {
        title: "Genre",
        title2: "Genre",
        required: "Genre requis",
      },
      age: {
        title: "Age *",
        title2: "Age",
        title3: "Valeur age",
        title4: "Valeur min age",
        title5: "Valeur max age",
        required: "Age requis",
      },
      hometown: {
        title: "Ville natale *",
        title2: "Ville",
        required: "Ville natale requis",
      },
      isActive: {
        title: "Statut",
        title2: "Activé",
        title3: "Visible",
        enable: {
          title: "Cliquer pour activer",
          message: "Êtes vous sure de vouloir activer cet utilisateur ?",
        },
        disable: {
          title: "Cliquer pour désactiver",
          message: "Êtes vous sure de vouloir désactiver cet utilisateur?",
        },
      },
      lastLogin: {
        title: "Dernière connexion",
      },
    },
    message: {
      new: "Nouveau utilisateur enregistré",
      update: "Utilisateur modifié",
      delete: {
        title: "Utilisateur suprimé",
        title2: "Êtes vous sure de vouloir supprimer cet utilisateur ?",
      },
      status: {
        title: "Utilisateur activé",
        title2: "Utilisateur désactivé",
        title3: "Confirmation de suppression",
      },
    },
  },
  account: {
    title: "Mon compte",
    subtitle: {
      title: "Mes informations",
    },
    tabs: {
      profile: {
        title: "Mes infos",
        title2: "Editer vos informations personnelles",
      },
      password: {
        title: "Sécurité",
        title2: "Changer votre mot de passe",
      },
    },
    cols: {
      created: {
        title: "Dernière connexion",
        title2: "Dernière mise à jour",
      },
      firstName: {
        title: "Nom *",
        length: "Nom doit contenir {min} à {max} caractères",
        required: "Nom requis",
      },
      lastName: {
        title: "Prénom",
      },
      password: {
        title: "Mot de passe *",
        required: "Mot de passe requis",
        length: "Mot de passe doit contenir {min} à {max} caractères",
      },
      gender: {
        title: "Genre",
        title2: "Genre",
        required: "Genre requis",
      },
      age: {
        title: "Age *",
        title2: "Age",
        required: "Age requis",
      },
      hometown: {
        title: "Ville natale *",
        title2: "Ville",
        required: "Ville natale requis",
      },
      oldPassword: {
        title: "Mot de passe actuel *",
        required: "Mot de passe actuel requis",
      },
      repeatPassword: {
        title: "Repéter le mot de passe *",
        sameAs: "Mot de passe saisie non conforme",
      },
    },
    message: {
      update: {
        title: "Profil modifié",
        title2: "Mot de passe changé",
      },
    },
  },
};
