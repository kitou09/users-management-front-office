import { createApp } from "vue";
import App from "./App.vue";
import { Quasar } from "quasar";
import quasarUserOptions from "./quasar-user-options";
import router from "./router";
import VueI18n from "./plugins/vue-i18n";
import vueMeta from "./plugins/vue-meta";
import store from "./store";
import vueAxios from "./plugins/vue-axios";
import auth from "./plugins/auth";

// fonts
import "notosans-fontface/scss/notosans-fontface.scss";

const app = createApp(App);

app.router = router;

app
  .use(vueAxios)
  .use(router)
  .use(auth)
  .use(store)
  .use(Quasar, quasarUserOptions)
  .use(VueI18n)
  .use(vueMeta)
  .mount("#app");
