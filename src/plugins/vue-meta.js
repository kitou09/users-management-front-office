import { createMetaManager } from "vue-meta/dist/vue-meta.esm-browser";

export default (app) => {
  app.use(createMetaManager());
};
