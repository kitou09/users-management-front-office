import { createI18n } from "vue-i18n";
import fr from "./../locale/fr";

export default (app) => {
  app.use(
    createI18n({
      locale: "fr", // set locale
      fallbackLocale: "fr", // set fallback locale
      messages: { fr },
    })
  );
};
