import axios from "axios";
import VueAxios from "vue-axios";

export default (app) => {
  app.use(VueAxios, axios);
  app.$http.defaults.baseURL =
    process.env.NODE_ENV === "production"
      ? window.location.origin
      : process.env.VUE_APP_BASE_URL;
  app.$http.defaults.headers.post["Content-Type"] = "application/json";
};
