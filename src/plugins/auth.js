import { createAuth } from "@websanova/vue-auth";
import driverAuthBearer from "./authbearer";
import driverHttpAxios from "@websanova/vue-auth/dist/drivers/http/axios.1.x.esm.js";
import driverRouterVueRouter from "@websanova/vue-auth/dist/drivers/router/vue-router.2.x.esm.js";
import URLPath from "../helpers/URL";

const tokenName = process.env.VUE_APP_TOKEN_NAME;

export default (app) => {
  app.use(
    createAuth({
      plugins: {
        http: app.axios,
        router: app.router,
      },
      drivers: {
        http: driverHttpAxios,
        auth: driverAuthBearer,
        router: driverRouterVueRouter,
      },
      options: {
        rolesKey: "role",
        notFoundRedirect: { name: "notfound" },
        tokenDefaultKey: tokenName,
        stores: ["storage"],
        authRedirect: {
          path: "/login",
        },
        forbiddenRedirect: {
          name: "forbidden",
        },
        fetchData: {
          url: URLPath.SECURITY.ME.URI,
          method: URLPath.SECURITY.ME.METHOD,
          enabled: true,
        },
        loginData: {
          url: URLPath.SECURITY.LOGIN.URI,
          method: URLPath.SECURITY.LOGIN.METHOD,
          redirect: "/",
          fetchUser: true,
        },
        logoutData: {
          url: URLPath.SECURITY.LOGOUT.URI,
          method: URLPath.SECURITY.LOGOUT.METHOD,
          redirect: "/login",
          makeRequest: true,
          transformRequest: function (data) {
            const token = localStorage.getItem("auth_token_impersonate")
              ? localStorage.getItem("auth_token_impersonate")
              : localStorage.getItem(tokenName);
            data = JSON.stringify({
              refresh_token: JSON.parse(token).refresh_token,
            });
            return data;
          },
        },
        refreshData: {
          url: URLPath.SECURITY.REFRESH.URI,
          method: URLPath.SECURITY.REFRESH.METHOD,
          enabled: true,
          transformRequest: function (data) {
            const token = localStorage.getItem(tokenName);
            data = JSON.stringify({
              refresh_token: JSON.parse(token).refresh_token,
            });
            return data;
          },
          interval: 20,
        },
        parseUserData: function (data) {
          return data || {};
        },
      },
    })
  );
};
