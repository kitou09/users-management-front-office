import { createWebHashHistory, createRouter } from "vue-router";
import Layout from "../views/Layout.vue";

const routes = [
  {
    path: "/",
    component: Layout,
    meta: {
      auth: true,
    },
    children: [
      {
        path: "/users",
        name: "user",
        component: () => import("../views/User/Index"),
      },
      {
        path: "/account",
        name: "account",
        component: () => import("../views/Account/Index"),
      },
    ],
  },
  {
    path: "/403",
    name: "forbidden",
    meta: {
      auth: true,
    },
    component: () => import("../views/Exception/403"),
  },
  {
    path: "/404",
    name: "notfound",
    component: () => import("../views/Exception/404"),
  },
  {
    path: "/login",
    component: () => import("../views/Security/Index.vue"),
    name: "login",
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
