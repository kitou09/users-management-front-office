export default {
  namespaced: true,
  state: {
    genders: ["Male", "Female"],
    operators: ["lt", "lte", "gt", "gte", "exact", "between"],
  },
  getters: {
    genders: (state) => {
      return state.genders;
    },
    operators: (state) => {
      return state.operators;
    },
  },
};
