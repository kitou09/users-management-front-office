import { createStore } from "vuex";
import * as types from "./mutations-type";
import user from "./modules/user";

const defaultNotification = {
  group: false,
  status: false,
  timeout: 6000,
  multiLine: false,
  progress: true,
  position: "bottom-left",
  color: "rgba(0, 0, 0, 1)",
  message: "",
  actions: [],
  closeBtn: true,
};

export default (app) => {
  app.use(
    createStore({
      state: {
        notification: defaultNotification,
      },
      getters: {
        notification: (state) => {
          return state.notification;
        },
      },
      mutations: {
        [types.NOTIFICATION](state, notification) {
          state.notification = notification;
        },
      },
      actions: {
        setNotification: (store, options) => {
          // clear old notification
          clearTimeout(store.getters.notification);

          // merge default notifications options with current options and set notification
          let currentOptions = Object.assign({}, defaultNotification, options);
          store.commit(types.NOTIFICATION, currentOptions);

          // set notification
          window.setTimeout(function () {
            store.commit(types.NOTIFICATION, defaultNotification);
          }, currentOptions.timeout);

          store.commit(types.NOTIFICATION, currentOptions);
        },
        request: (store, params) => {
          return new Promise((resolve, reject) => {
            app
              .$http({
                ...params,
              })
              .then((response) => {
                resolve(response);
              })
              .catch((error) => {
                reject(error);
              })
              .then(() => {
                resolve();
              });
          });
        },
      },
      modules: {
        user: user,
      },
    })
  );
};
