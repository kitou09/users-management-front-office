import * as validators from "@vuelidate/validators";
import { useI18n } from "vue-i18n";

// or import { createI18nMessage } from '@vuelidate/validators'
const { createI18nMessage } = validators;

// extract the `t` helper, should work for both Vue 2 and Vue 3 versions of vue-i18n
const { t } = useI18n.global || useI18n;

const messagePath = ({ $validator }) => `message.validations.${$validator}`;

// pass `t` and create your i18n message instance
const withI18nMessage = createI18nMessage({ t, messagePath });

// wrap each validator.
export const required = withI18nMessage(validators.required);

// validators that expect a parameter should have `{ withArguments: true }` passed as a second parameter, to annotate they should be wrapped
export const minLength = withI18nMessage(validators.minLength, {
  withArguments: true,
});

export const maxLength = withI18nMessage(validators.maxLength, {
  withArguments: true,
});

export const helpers = withI18nMessage(validators.helpers);

export const requiredIf = withI18nMessage(validators.requiredIf);

export const sameAs = withI18nMessage(validators.sameAs);
