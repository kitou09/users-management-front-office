let URLPath = {
  SECURITY: {
    LOGIN: {
      URI: "api/v1/login",
      METHOD: "POST",
    },
    LOGOUT: {
      URI: "api/v1/logout",
      METHOD: "POST",
    },
    ME: {
      URI: "api/v1/users/me",
      METHOD: "GET",
    },
    REFRESH: {
      URI: "api/v1/refresh-token",
      METHOD: "POST",
    },
  },
  USER: {
    LIST: "api/v1/users",
    CREATE: {
      URI: "api/v1/users",
      METHOD: "POST",
    },
    EDIT: {
      URI: "api/v1/users/{USER_ID}",
      METHOD: "PUT",
    },
    UNIQUE_USERNAME: {
      URI: "api/v1/users/username/exists",
      METHOD: "POST",
    },
    DELETE: {
      URI: "api/v1/users/{USER_ID}",
      METHOD: "DELETE",
    },
    CHANGE_PASSWORD: {
      URI: "api/v1/users/change-password",
      METHOD: "PUT",
    },
  },
  HOMETOWN: {
    LIST: "api/v1/hometowns",
  },
};

export default URLPath;
